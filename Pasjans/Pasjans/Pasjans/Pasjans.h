#pragma once
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <conio.h>
#include <time.h>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <iomanip>
#include <array>
#include <Windows.h>
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")

using namespace std;

int const TALIA = 53; //Bez indeksu 0
int const KARTY = 14; //Kart jest 13, ale indeks odpowie warto�ci

class CKarta
{
public:
	string _Symbol;
	int _Wartosc;
	friend class CTalia;
};

class CTalia
{
public:

	CKarta *_CzerwoneSerca = new CKarta[KARTY];
	CKarta *_CzarneSerce = new CKarta[KARTY];
	CKarta *_Karo = new CKarta[KARTY];
	CKarta *_Trefl = new CKarta[KARTY];

	CKarta *_Talia = new CKarta[TALIA];

	array <CKarta, KARTY> Stosik_A;

	array <CKarta, KARTY> Stosik_B;
	array <CKarta, KARTY> Stosik_C;
	array <CKarta, KARTY> Stosik_D;
	array <CKarta, KARTY> Stosik_E;

	array <CKarta, KARTY> Stosik_F;
	array <CKarta, KARTY> Stosik_G;
	array <CKarta, KARTY> Stosik_H;
	array <CKarta, KARTY> Stosik_I;
	array <CKarta, 35> Kolejka;

	//METODY
	CKarta InicjalizacjaTalii(CKarta Karta[], char nazwa);
	CKarta LaczenieWTalie(CKarta Trefl[], CKarta Pik[], CKarta Karo[], CKarta Serce[]);
	CKarta LosowanieTalii();
	CKarta Zerowanie_array();
	int Zliczanie_elementow_array(array <CKarta, KARTY> talia);
	int Zliczanie_elementow_array2(array <CKarta, 35> talia);
	CKarta Destrukcja_Niepotrzebnych_Tablic(CKarta Pomniejsze_Talie[]);
	CKarta Podzial_na_stosiki();
	CKarta Wypisanie_stosu_A(array <CKarta, KARTY> stosik);
	CKarta Stol_B(int x, int y, int nazwa_stosu = 2);
	CKarta Stol(array <CKarta, KARTY> Stos, int x, int y, int nazwa);
	CKarta Stol_3(array <CKarta, 35> Stos, int x, int y, int nazwa);
	CKarta Stol_2(array <CKarta, KARTY> Stos, int x, int y, int nazwa, int numer_z_Talia);
	void Rysuj_podzial(int x, int y);
	CKarta Ruch(int skad, int dokad);
	CTalia();
	CTalia(int ilosc_losowan);
	~CTalia();
	friend class CStos;
};
