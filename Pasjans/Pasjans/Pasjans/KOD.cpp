#include "Pasjans.h"

void Rozpoczecie();
void Zmiana_rozmiaru_okna();

int main()
{
	int a, b, c, d, e, f, g, h;
	a = 3; b = 4; c = 5; d = 15; e = 16; f = 17; g = 18; h = 10;
	Zmiana_rozmiaru_okna();
	Rozpoczecie();
	char menu;
	cin >> menu;
	
	for (;;) {
		switch (menu)
		{
		case '1':
		{
			system("cls");
			CTalia *pTalia = new CTalia(5);
			CTalia *p2Talia = new CTalia;
			CTalia *p3Talia = new CTalia;

			int Skad, Dokad;
			cout << "\n\nPo wpisaniu 0 0 gra zostanie zakonczona\n";
			cout << "\nSkad chcesz przelozyc karte? ";
			cin >> Skad;
			cout << "\nGdzie chcesz ja przelozyc? ";
			cin >> Dokad;
			p2Talia->Ruch(Skad, Dokad);
			do
			{
				a = a - 1; b = b - 1; c = c - 1; d = d - 1; e = e - 1; f = f - 1; g = g - 1; h = h - 1;
				system("cls");
				p2Talia->Wypisanie_stosu_A(p3Talia->Stosik_A);
				p2Talia->Stol_B(5, 5);
				p2Talia->Stol(p3Talia->Stosik_C, 12, 5, a);
				p2Talia->Stol(p3Talia->Stosik_D, 19, 5, b);
				p2Talia->Stol(p3Talia->Stosik_E, 26, 5, c);
				p2Talia->Rysuj_podzial(30, 5);
				p2Talia->Stol_2(p3Talia->Stosik_F, 31, 5, 6, d);
				p2Talia->Stol_2(p3Talia->Stosik_G, 38, 5, 7, e);
				p2Talia->Stol_2(p3Talia->Stosik_H, 45, 5, 8, f);
				p2Talia->Stol_2(p3Talia->Stosik_I, 52, 5, 9, g);
				p2Talia->Stol_3(p3Talia->Kolejka, 0, 20, h);
				cout << "\nSkad chcesz przelozyc karte? ";
				cin >> Skad;
				cout << "\nGdzie chcesz ja przelozyc? ";
				cin >> Dokad;
				p2Talia->Ruch(Skad, Dokad);
				_getch();
			} while (p2Talia->Stosik_B[13]._Wartosc != 0 && p2Talia->Stosik_C[13]._Wartosc != 0 && p2Talia->Stosik_D[13]._Wartosc != 0 && p2Talia->Stosik_E[13]._Wartosc != 0);
			
			system("cls");
			cout << "\n\nWygrales\n";

			delete pTalia;
			delete p2Talia;
			delete p3Talia;

			cout << "\nWcisniecie ENTER zakonczy program\n";
			_getch();
			exit; break;
		}
		case '2':
		{
			system("cls");
			cout << "Witamy w grze Demon!\n\n";

			cout << "Zasady gry Pasjans Demon sa bardzo proste � na gorze wyznaczone sa 4 pola, \n ale tym razem nie" <<
				"koniecznie na asy � na poczatku gry zawsze jedno z tych pol zostanie uzupelnione i bedziecie\n" <<
				"wiedziec jakie karty nalezy tam ukladac. Na karty, ktore umieszczone sa na tamtych polach nalezy\n" <<
				"klasc kolejne o oczko wyzsze i oczywiscie w tym samym kolorze. Jesli chodzi o karty na planszy\n" <<
				"to nalezy je przekladac na przemian kolorami czarny � czerwony i rzecz jasna karta o oczko\n" <<
				"mniejsza od tej, na ktora ja kladziemy. Karty, ktore przekladamy sa umieszczone w 4 kolumnach\n" <<
				"pod tymi w specjalnie wyznaczonych miejscach. Poza nimi sa jeszcze dwa stosy kart � jeden w lewym\n" <<
				"gornym rogu, z ktorego dobieramy karty i drugi nieco nizej, z ktorego karty same sa dobierane\n" <<
				"automatycznie ( cho� recznie tez mozemy ), kiedy na planszy jakas kolumna robi sie pusta. ";
			_getch();
			system("cls");
			main();
			break;
		}
		case '3':
		{
			system("cls");
			cout << "Gra zostala stworzona przez:\n";
			cout << " - Lorek Szymon\n";
			cout << " - Zahradnik Jakub\n";
			cout << "\nWcisniecie dowolnego klawisza spowoduje powrot do menu\n";
			_getch();
			system("cls");
			main();
		}
		case '4':
		{
			exit(0); break;
		}
		default:
		{
			cout << "\nNie ma takiej opcji. Kliknij ENTER aby kontynuowac.\n";
			getchar(); getchar();
			main();
		}
		}
	}
}

void Rozpoczecie()
{
	cout << "     @@@@@@@   @@@@@@@@  @@@@@@@@@@    @@@@@@   @@@  @@@" << endl << "     @@@@@@@@  @@@@@@@@  @@@@@@@@@@@  @@@@@@@@  @@@@ @@@" << endl << "     @@!  @@@  @@!       @@! @@! @@!  @@!  @@@  @@!@!@@@" << endl;
	cout << "     !@!  @!@  !@!       !@! !@! !@!  !@!  @!@  !@!!@!@!" << endl << "     @!@  !@!  @!!!:!    @!! !!@ @!@  @!@  !@!  @!@ !!@!" << endl << "     !@!  !!!  !!!!!:    !@!   ! !@!  !@!  !!!  !@!  !!!" << endl;
	cout << "     !!:  !!!  !!:       !!:     !!:  !!:  !!!  !!:  !!!" << endl << "     :!:  !:!  :!:       :!:     :!:  :!:  !:!  :!:  !:!" << endl << "      :::: ::   :: ::::  :::     ::   ::::: ::   ::   ::" << endl;
	cout << "     :: :  :   : :: ::    :      :     : :  :   ::    : " << endl;
	cout << endl << endl << "              `@                                @                    " << endl << "             `#,           `+@####@#.           @#                   " << endl;
	cout << "             @#          ##############         .#'                  " << endl << "            ###        ,########+#######;        ##.                 " << endl << "           .##@       ###+###############@       ###                 " << endl;
	cout << "           +##'      ######################      ###@                " << endl << "          #+##'     @#####################+'     @###,               " << endl << "          ####@    +#########################`   #####               " << endl;
	cout << "         .#####    #########################@,  `#####               " << endl << "         `######` @#+######################### ,+#####               " << endl << "          .##################################+#+#####                " << endl;
	cout << "          .##################################+#+#####                " << endl << "           ########################################+,                " << endl << "            @#################################+####+                 " << endl;
	cout << "             #####################################@`                 " << endl << "              ####################################                   " << endl << "               ######@#########################+#                    " << endl;
	cout << "                ##+##@#'###############+;#######                     " << endl << "                 ######;;'###########+;;;######                      " << endl << "                 ######;;;;;++#####+;;;;;######                      " << endl;
	cout << "                 :#+########++################@                      " << endl << "                  #+##########################                       " << endl << "                  ,##########################'                       " << endl;
	cout << "                   #########################@                        " << endl << "                    @#######################                         " << endl << "                     @#####################                          " << endl;
	cout << "                     `@@################@@                           " << endl << "                       +################@                            " << endl << endl << endl;
	cout << "1. Rozpocznij gre!" << endl;
	cout << "2. Instrukcja do gry." << endl;
	cout << "3. Tworcy." << endl;
	cout << "4. Wyjscie." << endl;
	PlaySound(TEXT("evil.wav"), NULL, SND_SYNC);
}

void Zmiana_rozmiaru_okna()
{
	srand(static_cast<unsigned int>(time(NULL)));
	system("cls");
	system("mode 169,52");
	SMALL_RECT WinRect = { 0, 0, 169, 52 };
	SMALL_RECT* WinSize = &WinRect;
	SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), true, WinSize);
}